import React from 'react';
import { Button, Form, InputGroup } from 'react-bootstrap';

class TurmasForm extends React.Component {
    constructor(props) 
    {
        super(props);
        this.state = {
            codigoTurma: '',
            quantidadeAlunos: 0,
    };

    this.handleCodigoTurmaChange = this.handleCodigoTurmaChange.bind(this);
    this.handleQuantidadeAlunosChange = this.handleQuantidadeAlunosChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleCodigoTurmaChange(event) {
    this.setState({
      codigoTurma: event.target.value
    });
  }

  handleQuantidadeAlunosChange(event) {
    this.setState({
      quantidadeAlunos: event.target.value
    });
  }

  handleSubmit(event) {
    alert('Nova turma cadastrada.: ' + JSON.stringify(this.state));
    this.setState({
        codigoTurma: '',
        quantidadeAlunos: 0
    });
    event.preventDefault();
  }

  render() {
    return (
      <Form onSubmit={this.handleSubmit}>

        <InputGroup className="mb-3">
            <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon1">Código da Turma:</InputGroup.Text>
            </InputGroup.Prepend>
            <Form.Control type="text" value={this.state.codigoTurma} 
                onChange={this.handleCodigoTurmaChange} />
        </InputGroup>

        <InputGroup className="mb-3">
            <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon1">Quantidade de Alunos:</InputGroup.Text>
            </InputGroup.Prepend>
            <Form.Control type="number"
            value={this.state.quantidadeAlunos} onChange={this.handleQuantidadeAlunosChange} />
        </InputGroup> 

        <br/>
        <Button variant="outline-primary" type="submit">Inserir Turma</Button>
      </Form>
    );
  }


}

export default TurmasForm;