import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import TurmasForm from './components/TurmasForm';
import './components/TurmasForm.css';
import logo from './components/img/logo.png';


function App() {
  return (
    <div>
      <header className="App-header">
        <img src={logo} alt="Controle de Notas"/>
        <p>Cadastro de Turmas</p>
        <br/>
        <TurmasForm />
      </header>
    </div>
  );
}


export default App;
