using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

public class Nota
{
    public int Id { get; set; }
    public Double Valor { get; set; }
    public int Acerto { get; set; }
    public int Erro { get; set; }
    public Aluno Aluno { get; set; }

}