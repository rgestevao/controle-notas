using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

public class Turma
{
    public int Id { get; set; }
    public string CodigoTurma { get; set; }
    public int QuantidadeAlunos { get; set; }
    public ICollection<Aluno> Alunos { get; set; }
    public ICollection<Professor> Professores { get; set; }
    public ICollection<Disciplina> Disciplinas { get; set; }
    
}