using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

public enum Status
{
    Baixo = 0, Intermediario = 1, Aceitável = 2, Ideal = 3
}