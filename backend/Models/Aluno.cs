using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

public class Aluno
{
    public int Id { get; set; }
    public string Nome { get; set; }
    public int Idade { get; set; }
    public int Cpf { get; set; }
    public ICollection<Professor> Professores { get; set; }
    public ICollection<Nota> Notas { get; set; }
    public ICollection<Disciplina> Disciplinas { get; set; }
    public Turma Turma { get; set; }

}