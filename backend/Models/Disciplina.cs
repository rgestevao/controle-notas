using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

public class Disciplina
{
   public int Id { get; set; }
   public string Nome { get; set; }
   public DateTime DataInicio { get; set; }
   public DateTime DataFim { get; set; }
   public Turma Turma { get; set; }
   public Nota Nota { get; set; }
   
}