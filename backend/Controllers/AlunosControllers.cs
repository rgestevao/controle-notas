using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using controle_notas.Models;

namespace controle_notas.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class AlunosController : ControllerBase
  {
    private readonly ApiDbContext _context;

    public AlunosController(ApiDbContext context)
    {
      _context = context;
    }

    // GET: api/Alunos
    [HttpGet]
    public ActionResult<IEnumerable<Aluno>> Get()
    {
      return _context.Alunos;
    }

    // GET: api/Alunos/5
    [HttpGet("{id}")]
    public ActionResult<Aluno> GetById(int id)
    {
      var item = _context.Alunos.Find(id);

      if (item == null)
      {
        return NotFound();
      }

      return item;
    }

    // POST: api/Alunos
    [HttpPost]
    public ActionResult<Aluno> Post(Aluno item)
    {
      _context.Alunos.Add(item);
      _context.SaveChanges();

      return CreatedAtAction(nameof(GetById), new { id = item.Id }, item);
    }

    // PUT: api/Alunos/5
    [HttpPut("{id}")]
    public IActionResult Put(int id, Aluno item)
    {
      if (id != item.Id)
      {
        return BadRequest();
      }

      _context.Entry(item).State = EntityState.Modified;
      _context.SaveChanges();

      return NoContent();
    }

    // DELETE: api/Alunos/5
    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
      var item = _context.Alunos.Find(id);

      if (item == null)
      {
        return NotFound();
      }

      _context.Alunos.Remove(item);
      _context.SaveChangesAsync();

      return NoContent();
    }
  }
}
