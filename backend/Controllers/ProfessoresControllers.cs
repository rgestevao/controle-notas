using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using controle_notas.Models;

namespace controle_notas.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class ProfessoresController : ControllerBase
  {
    private readonly ApiDbContext _context;

    public ProfessoresController(ApiDbContext context)
    {
      _context = context;
    }

    // GET: api/Professores
    [HttpGet]
    public ActionResult<IEnumerable<Professor>> Get()
    {
      return _context.Professores;
    }

    // GET: api/Professores/5
    [HttpGet("{id}")]
    public ActionResult<Professor> GetById(int id)
    {
      var item = _context.Professores.Find(id);

      if (item == null)
      {
        return NotFound();
      }

      return item;
    }

    // POST: api/Professores
    [HttpPost]
    public ActionResult<Professor> Post(Professor item)
    {
      _context.Professores.Add(item);
      _context.SaveChanges();

      return CreatedAtAction(nameof(GetById), new { id = item.Id }, item);
    }

    // PUT: api/Professores/5
    [HttpPut("{id}")]
    public IActionResult Put(int id, Professor item)
    {
      if (id != item.Id)
      {
        return BadRequest();
      }

      _context.Entry(item).State = EntityState.Modified;
      _context.SaveChanges();

      return NoContent();
    }

    // DELETE: api/Professores/5
    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
      var item = _context.Professores.Find(id);

      if (item == null)
      {
        return NotFound();
      }

      _context.Professores.Remove(item);
      _context.SaveChangesAsync();

      return NoContent();
    }
  }
}
