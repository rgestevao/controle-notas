using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using controle_notas.Models;

namespace controle_notas.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class NotasController : ControllerBase
  {
    private readonly ApiDbContext _context;

    public NotasController(ApiDbContext context)
    {
      _context = context;
    }

    // GET: api/Notas
    [HttpGet]
    public ActionResult<IEnumerable<Nota>> Get()
    {
      return _context.Notas;
    }

    // GET: api/Notas/5
    [HttpGet("{id}")]
    public ActionResult<Nota> GetById(int id)
    {
      var item = _context.Notas.Find(id);

      if (item == null)
      {
        return NotFound();
      }

      return item;
    }

    // POST: api/Notas
    [HttpPost]
    public ActionResult<Nota> Post(Nota item)
    {
      _context.Notas.Add(item);
      _context.SaveChanges();

      return CreatedAtAction(nameof(GetById), new { id = item.Id }, item);
    }

    // PUT: api/Notas/5
    [HttpPut("{id}")]
    public IActionResult Put(int id, Nota item)
    {
      if (id != item.Id)
      {
        return BadRequest();
      }

      _context.Entry(item).State = EntityState.Modified;
      _context.SaveChanges();

      return NoContent();
    }

    // DELETE: api/Notas/5
    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
      var item = _context.Notas.Find(id);

      if (item == null)
      {
        return NotFound();
      }

      _context.Notas.Remove(item);
      _context.SaveChangesAsync();

      return NoContent();
    }
  }
}
