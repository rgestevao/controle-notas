using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using controle_notas.Models;

namespace controle_notas.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class TurmasController : ControllerBase
  {
    private readonly ApiDbContext _context;

    public TurmasController(ApiDbContext context)
    {
      _context = context;
    }

    // GET: api/Turmas
    [HttpGet]
    public ActionResult<IEnumerable<Turma>> Get()
    {
      return _context.Turmas;
    }

    // GET: api/Turmas/5
    [HttpGet("{id}")]
    public ActionResult<Turma> GetById(int id)
    {
      var item = _context.Turmas.Find(id);

      if (item == null)
      {
        return NotFound();
      }

      return item;
    }

    // POST: api/Turmas
    [HttpPost]
    public ActionResult<Turma> Post(Turma item)
    {
      _context.Turmas.Add(item);
      _context.SaveChanges();

      return CreatedAtAction(nameof(GetById), new { id = item.Id }, item);
    }

    // PUT: api/Turmas/5
    [HttpPut("{id}")]
    public IActionResult Put(int id, Turma item)
    {
      if (id != item.Id)
      {
        return BadRequest();
      }

      _context.Entry(item).State = EntityState.Modified;
      _context.SaveChanges();

      return NoContent();
    }

    // DELETE: api/Turmas/5
    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
      var item = _context.Turmas.Find(id);

      if (item == null)
      {
        return NotFound();
      }

      _context.Turmas.Remove(item);
      _context.SaveChangesAsync();

      return NoContent();
    }
  }
}
