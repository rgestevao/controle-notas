using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using controle_notas.Models;

namespace controle_notas.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class DisciplinasController : ControllerBase
  {
    private readonly ApiDbContext _context;

    public DisciplinasController(ApiDbContext context)
    {
      _context = context;
    }

    // GET: api/Disciplina
    [HttpGet]
    public ActionResult<IEnumerable<Disciplina>> Get()
    {
      return _context.Disciplinas;
    }

    // GET: api/Disciplina/5
    [HttpGet("{id}")]
    public ActionResult<Disciplina> GetById(int id)
    {
      var item = _context.Disciplinas.Find(id);

      if (item == null)
      {
        return NotFound();
      }

      return item;
    }

    // POST: api/Disciplina
    [HttpPost]
    public ActionResult<Disciplina> Post(Disciplina item)
    {
      _context.Disciplinas.Add(item);
      _context.SaveChanges();

      return CreatedAtAction(nameof(GetById), new { id = item.Id }, item);
    }

    // PUT: api/Disciplina/5
    [HttpPut("{id}")]
    public IActionResult Put(int id, Disciplina item)
    {
      if (id != item.Id)
      {
        return BadRequest();
      }

      _context.Entry(item).State = EntityState.Modified;
      _context.SaveChanges();

      return NoContent();
    }

    // DELETE: api/Disciplina/5
    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
      var item = _context.Disciplinas.Find(id);

      if (item == null)
      {
        return NotFound();
      }

      _context.Disciplinas.Remove(item);
      _context.SaveChangesAsync();

      return NoContent();
    }
  }
}
